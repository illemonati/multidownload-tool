package main

import (
	"errors"
	"fmt"
	"github.com/fatih/color"
	"github.com/gosuri/uiprogress"
	"github.com/imroc/req"
	"math"
	"os"
	"regexp"
	"runtime"
	"strconv"
	"sync"
)

func resolveOutputNameFromUrl(url string) string {
	r := regexp.MustCompile(".+/(.+)")
	match := r.FindStringSubmatch(url)
	//fmt.Println(match)
	if len(match) < 2 {
		return "out"
	}
	return match[1]
}

func Download(url, outputName string) {
	if len(url) < 1 {
		return
	}
	if len(outputName) < 1 {
		outputName = resolveOutputNameFromUrl(url)
	}
	fmt.Printf("Downloading %s from %s\n", outputName, url)
	splittable, contentLength := parseHeaderToCheckIfSplittable(url)
	var err error
	if splittable {
		fmt.Println(color.GreenString("Content is splittable."))
		threads := runtime.NumCPU()
		err = downloadFile(url, outputName, contentLength, threads)
	} else {
		fmt.Println(color.GreenString("Content is not splittable."))
		err = downloadFile(url, outputName, contentLength, 1)
	}
	if err != nil {
		fmt.Println(err)
	}
}

func downloadFile(url, outputName string, length uint64, threads int) error {
	if threads < 1 || url == "" {
		return errors.New("incorrect params")
	}
	lengthEachPart := uint64(math.Ceil(float64(length) / float64(threads)))
	wg := sync.WaitGroup{}
	uiprogress.Start()
	var partRanges []uint64
	for i := uint64(0); i < length; i += lengthEachPart {
		wg.Add(1)
		partRanges = append(partRanges, i)
		i := i
		go func() {
			err := downloadFilePart(url, outputName, i, i+lengthEachPart-1, &wg)
			if err != nil {
				fmt.Println(err)
			}
		}()
	}
	wg.Wait()
	file, err := os.Create(outputName)
	if err != nil {
		return err
	}
	err = addTogether(file, partRanges, outputName)
	if err != nil {
		return err
	}
	return nil
}

func addTogether(file *os.File, partRanges []uint64, outputName string) error {
	defer file.Close()
	buffer := make([]byte, 1024)
	for _, start := range partRanges {
		partFileName := fmt.Sprintf("%s.%d.part", outputName, start)
		partFile, err := os.Open(partFileName)
		if err != nil {
			return err
		}
		for {
			n, err := partFile.Read(buffer)
			if n == 0 {
				break
			}
			if err != nil {
				return err
			}
			_, err = file.Seek(0, 2)
			if err != nil {
				return err
			}
			if _, err = file.Write(buffer[:n]); err != nil {
				return err
			}
		}
		err = partFile.Close()
		if err != nil {
			return err
		}
		err = os.Remove(partFileName)
		if err != nil {
			return err
		}
	}
	return nil
}

func downloadFilePart(url, outputName string, start, end uint64, wg *sync.WaitGroup) error {
	defer wg.Done()
	var header req.Header
	var bar *uiprogress.Bar
	if end > 0 {
		header = req.Header{
			"Range": fmt.Sprintf("bytes=%d-%d", start, end),
		}
		bar = uiprogress.AddBar(int(end - start))
	} else {
		bar = uiprogress.AddBar(0)
	}
	bar.AppendCompleted()
	bar.PrependElapsed()
	progress := func(current, total int64) {
		bar.Set(int(current))
	}
	resp, err := req.Get(url, header, req.DownloadProgress(progress))
	if err != nil {
		return err
	}
	err = resp.ToFile(fmt.Sprintf("%s.%d.part", outputName, start))
	if err != nil {
		return err
	}
	go bar.Set(int(end - start))
	return nil
}

func parseHeaderToCheckIfSplittable(url string) (splittable bool, length uint64) {
	r := req.New()
	resp, err := r.Get(url)
	if err != nil {
		return
	}
	header := resp.Response().Header
	acceptRanges := header.Get("Accept-Ranges")
	length, err = strconv.ParseUint(header.Get("Content-Length"), 10, 64)
	if err == nil && acceptRanges == "bytes" {
		splittable = true
	}
	return
}
