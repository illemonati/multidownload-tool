package main

import (
	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	url 		  	= kingpin.Arg("url", "Url to download from").Required().String()
	outputName		= kingpin.Flag("output", "Name of file to output").Short('o').String()
)

func main() {
	kingpin.Parse()
	Download(*url, *outputName)
}




