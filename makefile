SRC_DIR := ./src/
BUILD_DIR := ./build/
OUTNAME := multidownload-tool


PLATFORMS=darwin linux windows
ARCHITECTURES=386 amd64

ifeq ($(OS),Windows_NT)     # is Windows_NT on XP, 2000, 7, Vista, 10...
    setenv := set
else
    setenv := export
endif

default: all

build:
	$(foreach GOOS, $(PLATFORMS),\
		$(foreach GOARCH, $(ARCHITECTURES), $(shell $(setenv) GOOS=$(GOOS); $(setenv) GOARCH=$(GOARCH); go build --trimpath -v -ldflags "-s -w" -o build/$(GOOS)/$(GOOS)-$(GOARCH)/${OUTNAME}-$(GOOS)-$(GOARCH)$(if $(filter windows , $(GOOS)),.exe) $(SRC_DIR))))

all: clean build

clean:
	rm -rf build/

