module gitlab.com/illemonati/multidownload-tool

go 1.14

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751 // indirect
	github.com/alecthomas/units v0.0.0-20190924025748-f65c72e2690d // indirect
	github.com/fatih/color v1.9.0
	github.com/gosuri/uilive v0.0.4 // indirect
	github.com/gosuri/uiprogress v0.0.1
	github.com/imroc/req v0.3.0
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
)
